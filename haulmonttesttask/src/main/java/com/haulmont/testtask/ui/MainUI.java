package com.haulmont.testtask.ui;

import com.haulmont.testtask.api.TestTaskAPI;
import com.haulmont.testtask.model.impl.DoctorDTO;
import com.haulmont.testtask.model.impl.PatientDTO;
import com.haulmont.testtask.model.impl.PrescriptionDTO;
import com.vaadin.annotations.Theme;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.server.*;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import java.time.LocalDate;
import java.util.ArrayList;

@Theme(ValoTheme.THEME_NAME)
public class MainUI extends UI {

    private Table table;
    private MenuBar menuBar;
    private String tableName;
    private MenuBar.MenuItem menuItemFilter;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(false);
        setContent(layout);
        initAll(layout);
    }

    private void initAll(VerticalLayout layout) {
        initMenuBar(layout);
        initTable(layout);
    }

    private void initTable(Layout parentLayout) {
        table = new Table();
        tableName = "Null";
        parentLayout.addComponent(table);
    }

    private void initMenuBar(Layout parentLayout) {
        menuBar = new MenuBar();
        menuBar.setWidth("100%");

        parentLayout.addComponent(menuBar);

        final MenuBar.MenuItem patientsMenuItem = menuBar.addItem("Пациенты", null, new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem selectedItem) {
                showTablePatients(parentLayout);
            }
        });
        final MenuBar.MenuItem doctorsMenuItem1 = menuBar.addItem("Врачи", null, new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem selectedItem) {
                showTableDoctors(parentLayout);
            }
        });
        final MenuBar.MenuItem prescriptionsMenuItem1 = menuBar.addItem("Рецепты", null, new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem selectedItem) {
                showTablePrescriptions(parentLayout);
            }
        });

        final MenuBar.MenuItem createMenuItem1 = menuBar.addItem("Добавить", null, new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem selectedItem) {
                switch (tableName) {
                    case "Patients":
                        createPatient(parentLayout);
                        break;
                    case "Doctors":
                        createDoctor(parentLayout);
                        break;
                    case "Prescriptions":
                        createPrescription(parentLayout);
                        break;
                }
            }
        });
        final MenuBar.MenuItem deleteMenuItem1 = menuBar.addItem("Удалить", null, new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem selectedItem) {
                switch (tableName) {
                    case "Patients":
                        deletePatient(parentLayout);
                        break;
                    case "Doctors":
                        deleteDoctor(parentLayout);
                        break;
                    case "Prescriptions":
                        deletePrescription(parentLayout);
                        break;
                }
            }
        });
        final MenuBar.MenuItem editMenuItem1 = menuBar.addItem("Изменить", null, new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem selectedItem) {
                switch (tableName) {
                    case "Patients":
                        editPatient(parentLayout);
                        break;
                    case "Doctors":
                        editDoctor(parentLayout);
                        break;
                    case "Prescriptions":
                        editPrescription(parentLayout);
                        break;
                }
            }
        });

    }

    private void showTablePatients(Layout parentLayout) {
        tableName = "Patients";
        parentLayout.removeComponent(table);
        PatientDTO[] patientDTOS = TestTaskAPI.patients();

        BeanContainer<Integer, PatientDTO> patientDTOBeanContainer = new BeanContainer<>(PatientDTO.class);
        patientDTOBeanContainer.setBeanIdProperty("id");
        for (int i = 0; i < patientDTOS.length; i++) {
            patientDTOBeanContainer.addBean(patientDTOS[i]);
        }

        table = new Table("Количество пациентов: " + patientDTOS.length, patientDTOBeanContainer);
        table.setPageLength(table.size());
        table.setWidth("100%");

        parentLayout.addComponent(table);

        if (menuItemFilter != null)
            menuBar.removeItem(menuItemFilter);
    }

    private void showTableDoctors(Layout parentLayout) {
        tableName = "Doctors";
        parentLayout.removeComponent(table);
        DoctorDTO[] doctorDTOS = TestTaskAPI.doctors();

        BeanContainer<Integer, DoctorDTO> patientDTOBeanContainer = new BeanContainer<>(DoctorDTO.class);
        patientDTOBeanContainer.setBeanIdProperty("id");
        for (int i = 0; i < doctorDTOS.length; i++) {
            patientDTOBeanContainer.addBean(doctorDTOS[i]);
        }

        table = new Table("Количество врачей: " + doctorDTOS.length, patientDTOBeanContainer);
        table.setPageLength(table.size());
        table.setWidth("100%");

        parentLayout.addComponent(table);

        if (menuItemFilter != null)
            menuBar.removeItem(menuItemFilter);
    }

    private void showTablePrescriptions(Layout parentLayout) {
        if (menuItemFilter != null)
            menuBar.removeItem(menuItemFilter);

        tableName = "Prescriptions";
        parentLayout.removeComponent(table);
        PrescriptionDTO[] prescriptionDTOS = TestTaskAPI.prescriptions();

        BeanContainer<Integer, PrescriptionDTO> prescriptionDTOBeanContainer = new BeanContainer<>(PrescriptionDTO.class);
        prescriptionDTOBeanContainer.setBeanIdProperty("id");
        for (int i = 0; i < prescriptionDTOS.length; i++) {
            prescriptionDTOBeanContainer.addBean(prescriptionDTOS[i]);
        }

        table = new Table("Количество рецептов: " + prescriptionDTOS.length, prescriptionDTOBeanContainer);
        table.setPageLength(table.size());
        table.setWidth("100%");

        parentLayout.addComponent(table);

        menuItemFilter = menuBar.addItem("Фильтер", null, new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem selectedItem) {
                Window subWindow = new Window("Фильтер");
                VerticalLayout content = new VerticalLayout();
                content.setMargin(true);
                subWindow.setWidth("50%");
                subWindow.setHeight("70%");
                subWindow.setContent(content);
                HorizontalLayout contentBottom = new HorizontalLayout();
                contentBottom.setMargin(true);
                contentBottom.setSpacing(true);

                TextField textFieldDescription = new TextField("Описание");
                textFieldDescription.setWidth("100%");
                TextField textFieldID = new TextField("ID Пациента");
                textFieldID.setWidth("100%");
                TextField textFieldPriority = new TextField("Приоритет");
                textFieldPriority.setWidth("100%");

                content.addComponent(textFieldDescription);
                content.addComponent(textFieldID);
                content.addComponent(textFieldPriority);

                Button buttonOk = new Button("Ок");
                buttonOk.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {


                        if (validatorText(textFieldDescription.getValue()) &&
                                validatorText(textFieldID.getValue()) &&
                                validatorText(textFieldPriority.getValue())) {
                            PrescriptionDTO[] prescriptionDTOS = TestTaskAPI.prescriptions();
                            ArrayList<PrescriptionDTO> prescriptionDTOArrayList = new ArrayList<>();
                            for (PrescriptionDTO dto : prescriptionDTOS) {
                                if (dto.getIdPatient() == Long.parseLong(textFieldID.getValue()) &&
                                        dto.getDescription().equals(textFieldDescription.getValue()) &&
                                        dto.getPriority().equals(textFieldPriority.getValue())) {
                                    prescriptionDTOArrayList.add(dto);
                                }
                            }
                            parentLayout.removeComponent(table);

                            BeanContainer<Integer, PrescriptionDTO> prescriptionDTOBeanContainer = new BeanContainer<>(PrescriptionDTO.class);
                            prescriptionDTOBeanContainer.setBeanIdProperty("id");

                            for (PrescriptionDTO prescriptionDTO : prescriptionDTOArrayList) {
                                prescriptionDTOBeanContainer.addBean(prescriptionDTO);
                            }

                            table = new Table("Количество рецептов: " + prescriptionDTOArrayList.size(), prescriptionDTOBeanContainer);
                            table.setPageLength(table.size());
                            table.setWidth("100%");
                            parentLayout.addComponent(table);
                            subWindow.close();
                        } else content.addComponent(new Label("Ошибка ввода данных"));
                    }
                });
                Button buttonCancel = new Button("Отмена");
                buttonCancel.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {
                        subWindow.close();
                    }
                });

                contentBottom.addComponent(buttonOk);
                contentBottom.addComponent(buttonCancel);

                content.addComponent(contentBottom);
                subWindow.setContent(content);
                subWindow.setModal(true);
                subWindow.center();
                addWindow(subWindow);
            }
        });

    }

    private void createPatient(Layout parentLayout) {
        Window subWindow = new Window("createPatient");
        VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        subWindow.setWidth("50%");
        subWindow.setHeight("70%");
        subWindow.setContent(content);
        HorizontalLayout contentBottom = new HorizontalLayout();
        contentBottom.setMargin(true);
        contentBottom.setSpacing(true);

        TextField textFieldFirstName = new TextField("Имя");
        textFieldFirstName.setWidth("100%");
        TextField textFieldLastName = new TextField("Фамилия");
        textFieldLastName.setWidth("100%");
        TextField textFieldFatherName = new TextField("Отчество");
        textFieldFatherName.setWidth("100%");
        TextField textFieldPhone = new TextField("Телефон (8 цифр)");
        textFieldPhone.setWidth("100%");

        content.addComponent(textFieldFirstName);
        content.addComponent(textFieldLastName);
        content.addComponent(textFieldFatherName);
        content.addComponent(textFieldPhone);

        Button buttonOk = new Button("Ок");
        buttonOk.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (validatorText(textFieldFirstName.getValue()) &&
                        validatorText(textFieldLastName.getValue()) &&
                        validatorText(textFieldFatherName.getValue()) &&
                        validatorPhone(textFieldPhone.getValue())) {
                    TestTaskAPI.createPatient(textFieldFirstName.getValue(),
                            textFieldLastName.getValue(),
                            textFieldFatherName.getValue(),
                            textFieldPhone.getValue()
                    );
                    subWindow.close();
                    showTablePatients(parentLayout);
                } else content.addComponent(new Label("Ошибка ввода данных"));
            }
        });
        Button buttonCancel = new Button("Отмена");
        buttonCancel.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.close();
            }
        });

        contentBottom.addComponent(buttonOk);
        contentBottom.addComponent(buttonCancel);

        content.addComponent(contentBottom);
        subWindow.setContent(content);
        subWindow.setModal(true);
        subWindow.center();
        addWindow(subWindow);
    }

    private void deletePatient(Layout parentLayout) {
        Window subWindow = new Window("deletePatient");
        VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        subWindow.setWidth("50%");
        subWindow.setHeight("30%");
        subWindow.setContent(content);
        HorizontalLayout contentBottom = new HorizontalLayout();
        contentBottom.setMargin(true);
        contentBottom.setSpacing(true);

        TextField textFieldID = new TextField("ID");
        textFieldID.setWidth("100%");
        content.addComponent(textFieldID);

        Button buttonOk = new Button("Ок");
        buttonOk.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (validatorID(textFieldID.getValue())) {
                    TestTaskAPI.deletePatient(textFieldID.getValue());

                    subWindow.close();
                    showTablePatients(parentLayout);
                } else content.addComponent(new Label("Ошибка ввода данных"));
            }
        });
        Button buttonCancel = new Button("Отмена");
        buttonCancel.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.close();
            }
        });

        contentBottom.addComponent(buttonOk);
        contentBottom.addComponent(buttonCancel);

        content.addComponent(contentBottom);
        subWindow.setContent(content);
        subWindow.setModal(true);
        subWindow.center();
        addWindow(subWindow);
    }

    private void editPatient(Layout parentLayout) {
        Window subWindow = new Window("editPatient");
        VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        subWindow.setWidth("50%");
        subWindow.setHeight("70%");
        subWindow.setContent(content);
        HorizontalLayout contentBottom = new HorizontalLayout();
        contentBottom.setMargin(true);
        contentBottom.setSpacing(true);

        TextField textFieldID = new TextField("ID");
        textFieldID.setWidth("100%");
        TextField textFieldFirstName = new TextField("Имя");
        textFieldFirstName.setWidth("100%");
        TextField textFieldLastName = new TextField("Фамилия");
        textFieldLastName.setWidth("100%");
        TextField textFieldFatherName = new TextField("Отчество");
        textFieldFatherName.setWidth("100%");
        TextField textFieldPhone = new TextField("Телефон (8 цифр)");
        textFieldPhone.setWidth("100%");

        content.addComponent(textFieldID);
        content.addComponent(textFieldFirstName);
        content.addComponent(textFieldLastName);
        content.addComponent(textFieldFatherName);
        content.addComponent(textFieldPhone);

        Button buttonOk = new Button("Ок");
        buttonOk.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (validatorID(textFieldID.getValue()) &&
                        validatorText(textFieldFirstName.getValue()) &&
                        validatorText(textFieldLastName.getValue()) &&
                        validatorText(textFieldFatherName.getValue()) &&
                        validatorPhone(textFieldPhone.getValue())) {
                    TestTaskAPI.editPatient(textFieldID.getValue(),
                            textFieldFirstName.getValue(),
                            textFieldLastName.getValue(),
                            textFieldFatherName.getValue(),
                            textFieldPhone.getValue()
                    );
                    subWindow.close();
                    showTablePatients(parentLayout);
                } else content.addComponent(new Label("Ошибка ввода данных"));
            }
        });
        Button buttonCancel = new Button("Отмена");
        buttonCancel.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.close();
            }
        });

        contentBottom.addComponent(buttonOk);
        contentBottom.addComponent(buttonCancel);

        content.addComponent(contentBottom);
        subWindow.setContent(content);
        subWindow.setModal(true);
        subWindow.center();
        addWindow(subWindow);
    }

    private void createDoctor(Layout parentLayout) {
        Window subWindow = new Window("createDoctor");
        VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        subWindow.setWidth("50%");
        subWindow.setHeight("70%");
        subWindow.setContent(content);
        HorizontalLayout contentBottom = new HorizontalLayout();
        contentBottom.setMargin(true);
        contentBottom.setSpacing(true);

        TextField textFieldFirstName = new TextField("Имя");
        textFieldFirstName.setWidth("100%");
        TextField textFieldLastName = new TextField("Фамилия");
        textFieldLastName.setWidth("100%");
        TextField textFieldFatherName = new TextField("Отчество");
        textFieldFatherName.setWidth("100%");
        TextField textFieldSpec = new TextField("Специализация");
        textFieldSpec.setWidth("100%");

        content.addComponent(textFieldFirstName);
        content.addComponent(textFieldLastName);
        content.addComponent(textFieldFatherName);
        content.addComponent(textFieldSpec);

        Button buttonOk = new Button("Ок");
        buttonOk.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (validatorText(textFieldFirstName.getValue()) &&
                        validatorText(textFieldLastName.getValue()) &&
                        validatorText(textFieldFatherName.getValue()) &&
                        validatorText(textFieldSpec.getValue())) {
                    TestTaskAPI.createDoctor(textFieldFirstName.getValue(),
                            textFieldLastName.getValue(),
                            textFieldFatherName.getValue(),
                            textFieldSpec.getValue()
                    );
                    subWindow.close();
                    showTableDoctors(parentLayout);
                } else content.addComponent(new Label("Ошибка ввода данных"));
            }
        });
        Button buttonCancel = new Button("Отмена");
        buttonCancel.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.close();
            }
        });

        contentBottom.addComponent(buttonOk);
        contentBottom.addComponent(buttonCancel);

        content.addComponent(contentBottom);
        subWindow.setContent(content);
        subWindow.setModal(true);
        subWindow.center();
        addWindow(subWindow);
    }

    private void deleteDoctor(Layout parentLayout) {
        Window subWindow = new Window("deleteDoctor");
        VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        subWindow.setWidth("50%");
        subWindow.setHeight("30%");
        subWindow.setContent(content);
        HorizontalLayout contentBottom = new HorizontalLayout();
        contentBottom.setMargin(true);
        contentBottom.setSpacing(true);

        TextField textFieldID = new TextField("ID");
        textFieldID.setWidth("100%");
        content.addComponent(textFieldID);

        Button buttonOk = new Button("Ок");
        buttonOk.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (validatorID(textFieldID.getValue())) {
                    TestTaskAPI.deleteDoctor(textFieldID.getValue());
                    subWindow.close();
                    showTableDoctors(parentLayout);
                } else content.addComponent(new Label("Ошибка ввода данных"));
            }
        });
        Button buttonCancel = new Button("Отмена");
        buttonCancel.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.close();
            }
        });

        contentBottom.addComponent(buttonOk);
        contentBottom.addComponent(buttonCancel);

        content.addComponent(contentBottom);
        subWindow.setContent(content);
        subWindow.setModal(true);
        subWindow.center();
        addWindow(subWindow);
    }

    private void editDoctor(Layout parentLayout) {
        Window subWindow = new Window("editDoctor");
        VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        subWindow.setWidth("50%");
        subWindow.setHeight("70%");
        subWindow.setContent(content);
        HorizontalLayout contentBottom = new HorizontalLayout();
        contentBottom.setMargin(true);
        contentBottom.setSpacing(true);

        TextField textFieldID = new TextField("ID");
        textFieldID.setWidth("100%");
        TextField textFieldFirstName = new TextField("Имя");
        textFieldFirstName.setWidth("100%");
        TextField textFieldLastName = new TextField("Фамилия");
        textFieldLastName.setWidth("100%");
        TextField textFieldFatherName = new TextField("Отчество");
        textFieldFatherName.setWidth("100%");
        TextField textFieldSpec = new TextField("Специализация");
        textFieldSpec.setWidth("100%");

        content.addComponent(textFieldID);
        content.addComponent(textFieldFirstName);
        content.addComponent(textFieldLastName);
        content.addComponent(textFieldFatherName);
        content.addComponent(textFieldSpec);

        Button buttonOk = new Button("Ок");
        buttonOk.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (validatorID(textFieldID.getValue()) &&
                        validatorText(textFieldFirstName.getValue()) &&
                        validatorText(textFieldLastName.getValue()) &&
                        validatorText(textFieldFatherName.getValue()) &&
                        validatorText(textFieldSpec.getValue())) {
                    TestTaskAPI.editDoctor(textFieldID.getValue(),
                            textFieldFirstName.getValue(),
                            textFieldLastName.getValue(),
                            textFieldFatherName.getValue(),
                            textFieldSpec.getValue()
                    );
                    subWindow.close();
                    showTableDoctors(parentLayout);
                } else content.addComponent(new Label("Ошибка ввода данных"));
            }
        });
        Button buttonCancel = new Button("Отмена");
        buttonCancel.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.close();
            }
        });

        contentBottom.addComponent(buttonOk);
        contentBottom.addComponent(buttonCancel);

        content.addComponent(contentBottom);
        subWindow.setContent(content);
        subWindow.setModal(true);
        subWindow.center();
        addWindow(subWindow);
    }

    private void createPrescription(Layout parentLayout) {
        Window subWindow = new Window("createPrescription");
        VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        subWindow.setWidth("50%");
        subWindow.setHeight("70%");
        subWindow.setContent(content);
        HorizontalLayout contentBottom = new HorizontalLayout();
        contentBottom.setMargin(true);
        contentBottom.setSpacing(true);

        TextField textFieldDescription = new TextField("Описание");
        textFieldDescription.setWidth("100%");
        TextField textFieldIdPatient = new TextField("ID Пациента");
        textFieldIdPatient.setWidth("100%");
        TextField textFieldIdDoctor = new TextField("ID Врача");
        textFieldIdDoctor.setWidth("100%");
        TextField textFieldTime = new TextField("Срок действия (дни)");
        textFieldTime.setWidth("100%");
        TextField textFieldPriority = new TextField("Приоритет (Normal, Cito, Statim)");
        textFieldPriority.setWidth("100%");

        content.addComponent(textFieldDescription);
        content.addComponent(textFieldIdPatient);
        content.addComponent(textFieldIdDoctor);
        content.addComponent(textFieldTime);
        content.addComponent(textFieldPriority);

        Button buttonOk = new Button("Ок");
        buttonOk.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (validatorText(textFieldDescription.getValue()) &&
                        validatorID(textFieldIdPatient.getValue()) &&
                        validatorID(textFieldIdDoctor.getValue()) &&
                        validatorID(textFieldTime.getValue()) &&
                        validatorPriority(textFieldPriority.getValue())) {
                    TestTaskAPI.createPrescription(textFieldDescription.getValue(),
                            textFieldIdPatient.getValue(),
                            textFieldIdDoctor.getValue(),
                            LocalDate.now(),
                            textFieldTime.getValue(),
                            textFieldPriority.getValue()
                    );
                    subWindow.close();
                    showTablePrescriptions(parentLayout);
                } else content.addComponent(new Label("Ошибка ввода данных"));
            }
        });
        Button buttonCancel = new Button("Отмена");
        buttonCancel.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.close();
            }
        });

        contentBottom.addComponent(buttonOk);
        contentBottom.addComponent(buttonCancel);

        content.addComponent(contentBottom);
        subWindow.setContent(content);
        subWindow.setModal(true);
        subWindow.center();
        addWindow(subWindow);
    }

    private void deletePrescription(Layout parentLayout) {
        Window subWindow = new Window("deletePrescription");
        VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        subWindow.setWidth("50%");
        subWindow.setHeight("30%");
        subWindow.setContent(content);
        HorizontalLayout contentBottom = new HorizontalLayout();
        contentBottom.setMargin(true);
        contentBottom.setSpacing(true);

        TextField textFieldID = new TextField("ID");
        textFieldID.setWidth("100%");
        content.addComponent(textFieldID);

        Button buttonOk = new Button("Ок");
        buttonOk.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (validatorID(textFieldID.getValue())) {
                    TestTaskAPI.deletePrescription(textFieldID.getValue());
                    subWindow.close();
                    showTablePrescriptions(parentLayout);
                } else content.addComponent(new Label("Ошибка ввода данных"));
            }
        });
        Button buttonCancel = new Button("Отмена");
        buttonCancel.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.close();
            }
        });

        contentBottom.addComponent(buttonOk);
        contentBottom.addComponent(buttonCancel);

        content.addComponent(contentBottom);
        subWindow.setContent(content);
        subWindow.setModal(true);
        subWindow.center();
        addWindow(subWindow);
    }

    private void editPrescription(Layout parentLayout) {
        Window subWindow = new Window("editPrescription");
        VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        subWindow.setWidth("50%");
        subWindow.setHeight("80%");
        subWindow.setContent(content);
        HorizontalLayout contentBottom = new HorizontalLayout();
        contentBottom.setMargin(true);
        contentBottom.setSpacing(true);

        TextField textFieldID = new TextField("ID");
        textFieldID.setWidth("100%");
        TextField textFieldDescription = new TextField("Описание");
        textFieldDescription.setWidth("100%");
        TextField textFieldIdPatient = new TextField("ID Пациента");
        textFieldIdPatient.setWidth("100%");
        TextField textFieldIdDoctor = new TextField("ID Врача");
        textFieldIdDoctor.setWidth("100%");
        TextField textFieldTime = new TextField("Срок действия (дни)");
        textFieldTime.setWidth("100%");
        TextField textFieldPriority = new TextField("Приоритет (Normal, Cito, Statim)");
        textFieldPriority.setWidth("100%");

        content.addComponent(textFieldID);
        content.addComponent(textFieldDescription);
        content.addComponent(textFieldIdPatient);
        content.addComponent(textFieldIdDoctor);
        content.addComponent(textFieldTime);
        content.addComponent(textFieldPriority);

        Button buttonOk = new Button("Ок");
        buttonOk.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (validatorID(textFieldID.getValue()) &&
                        validatorText(textFieldDescription.getValue()) &&
                        validatorID(textFieldIdPatient.getValue()) &&
                        validatorID(textFieldIdDoctor.getValue()) &&
                        validatorID(textFieldTime.getValue()) &&
                        validatorPriority(textFieldPriority.getValue())) {
                    TestTaskAPI.editPrescription(textFieldID.getValue(),
                            textFieldDescription.getValue(),
                            textFieldIdPatient.getValue(),
                            textFieldIdDoctor.getValue(),
                            LocalDate.now(),
                            textFieldTime.getValue(),
                            textFieldPriority.getValue()
                    );
                    subWindow.close();
                    showTablePrescriptions(parentLayout);
                } else content.addComponent(new Label("Ошибка ввода данных"));
            }
        });
        Button buttonCancel = new Button("Отмена");
        buttonCancel.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.close();
            }
        });

        contentBottom.addComponent(buttonOk);
        contentBottom.addComponent(buttonCancel);

        content.addComponent(contentBottom);
        subWindow.setContent(content);
        subWindow.setModal(true);
        subWindow.center();
        addWindow(subWindow);
    }

    private boolean validatorText(String text) {
        return text.length() <= 30;
    }

    private boolean validatorPhone(String text) {
        return text.matches("\\d+") && text.length() < 9;
    }

    private boolean validatorID(String text) {
        return text.matches("\\d+") && Integer.parseInt(text) >= 0;
    }

    private boolean validatorPriority(String text) {
        return text.equals("Cito") || text.equals("Normal") || text.equals("Statim");
    }
}