package com.haulmont.testtask.model.impl;

import com.haulmont.testtask.model.DAO;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class DAOimpl implements DAO {

    private static DAOimpl instance;
    private Connection connection = null;


    public static synchronized DAOimpl getInstance() {
        if (instance == null) {
            instance = new DAOimpl();
            instance.loadDriver();
            instance.getConnection();
            instance.createTable();
        }
        return instance;
    }


    private boolean loadDriver() {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean getConnection() {
        try {
            String path = "./src/main/resources/";
            String dbname = "mydb";
            String connectionString = "jdbc:hsqldb:file:"+path+dbname;
            String login = "sa";
            String password = "";
            connection = DriverManager.getConnection(connectionString, login, password);

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void createTable() {
        try {
            Statement statement = connection.createStatement();
            String str = new String(Files.readAllBytes(Paths.get(".\\src\\main\\resources\\hsqldbScript.txt")));
            statement.executeUpdate(str);
        } catch (SQLException e) {

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeConnection() {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "SHUTDOWN";
            statement.execute(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        closeConnection();
    }

    @Override
    public PatientDTO[] patients() {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "SELECT * FROM patients";

            ResultSet resultSet = statement.executeQuery(sql);
            ArrayList<PatientDTO> patientDTOS = new ArrayList<>();
            while (resultSet.next()) {
                patientDTOS.add(new PatientDTO(resultSet.getLong(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getString(4), resultSet.getString(5)));
            }
            return patientDTOS.toArray(new PatientDTO[patientDTOS.size()]);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean createPatient(String firstName, String lastName, String fatherName, String phone) {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "INSERT INTO patients (firstName, lastName, fatherName, phone) VALUES('" + firstName + "', '" +
                    lastName + "', '" + fatherName + "', '" + phone + "')";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean editPatient(long id, String firstName, String lastName, String fatherName, String phone) {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "UPDATE patients SET firstName = '" + firstName + "', lastName = '" + lastName + "', fatherName = '" + fatherName
                    + "', phone = '" + phone + "' WHERE id = " + id;
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean deletePatient(long id) {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "DELETE FROM patients WHERE id = " + id;
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public DoctorDTO[] doctors() {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "SELECT * FROM doctors";

            ResultSet resultSet = statement.executeQuery(sql);
            ArrayList<DoctorDTO> doctorDTOS = new ArrayList<>();
            while (resultSet.next()) {
                doctorDTOS.add(new DoctorDTO(resultSet.getLong(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getString(4), resultSet.getString(5)));
            }
            return doctorDTOS.toArray(new DoctorDTO[doctorDTOS.size()]);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean createDoctor(String firstName, String lastName, String fatherName, String specification) {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "INSERT INTO doctors (firstName, lastName, fatherName, phone) VALUES('" + firstName + "', '" +
                    lastName + "', '" + fatherName + "', '" + specification + "')";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean editDoctor(long id, String firstName, String lastName, String fatherName, String specification) {
        Statement statement;
        try {
            statement = connection.createStatement();

            String sql = "UPDATE doctors SET firstName = '" + firstName + "', lastName = '" + lastName + "', fatherName = '" + fatherName
                    + "', phone = '" + specification + "' WHERE id = " + id;
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteDoctor(long id) {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "DELETE FROM doctors WHERE id = " + id;
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public PrescriptionDTO[] prescriptions() {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "SELECT * FROM prescriptions";
            ResultSet resultSet = statement.executeQuery(sql);
            ArrayList<PrescriptionDTO> prescriptionDTOS = new ArrayList<>();
            LocalDate localDate;
            while (resultSet.next()) {
                localDate = LocalDate.parse(resultSet.getString(5));
                prescriptionDTOS.add(new PrescriptionDTO(resultSet.getLong(1), resultSet.getString(2), resultSet.getLong(3),
                        resultSet.getLong(4), localDate, resultSet.getInt(6)
                        , resultSet.getString(7)));
            }
            return prescriptionDTOS.toArray(new PrescriptionDTO[prescriptionDTOS.size()]);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean createPrescription(String description, long idPatient, long idDoctor, LocalDate dateStr, int time, String priority) {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "INSERT INTO prescriptions (description, idPatient, idDoctor, dateStr, timeS, priority) VALUES('" + description + "', '" +
                   idPatient + "', '" + idDoctor + "', '" + dateStr.toString() + "', " + time + ", '" + priority + "')";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean editPrescription(long id, String description, long idPatient, long idDoctor, LocalDate dateStr, int time, String priority) {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "UPDATE prescriptions SET description = '" + description + "', idPatient = " + idPatient + ", idDoctor = " + idDoctor
                    + ", dateStr = '" + dateStr.toString()  + "', timeS = " + time  + ", priority = '" + priority + "' WHERE id = " + id;
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean deletePrescription(long id) {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "DELETE FROM prescriptions WHERE id = " + id;
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
