package com.haulmont.testtask.model;

import com.haulmont.testtask.model.impl.DoctorDTO;
import com.haulmont.testtask.model.impl.PatientDTO;
import com.haulmont.testtask.model.impl.PrescriptionDTO;

import java.time.LocalDate;

public interface DAO {

    PatientDTO[] patients();

    boolean createPatient(String firstName, String lastName, String fatherName, String phone);

    boolean editPatient(long id, String firstName, String lastName, String fatherName, String phone);

    boolean deletePatient(long id);

    DoctorDTO[] doctors();

    boolean createDoctor(String firstName, String lastName, String fatherName, String specification);

    boolean editDoctor(long id, String firstName, String lastName, String fatherName, String specification);

    boolean deleteDoctor(long id);

    PrescriptionDTO[] prescriptions();

    boolean createPrescription(String description, long idPatient, long idDoctor, LocalDate dateStr, int time, String priority);

    boolean editPrescription(long id, String description, long idPatient, long idDoctor, LocalDate dateStr, int time, String priority);

    boolean deletePrescription(long id);

}
