package com.haulmont.testtask.model.impl;

import java.time.LocalDate;

public class PrescriptionDTO {

    String description;
    long id;
    long idPatient;
    long idDoctor;
    LocalDate dateStr;
    int time;
    String priority;

    public PrescriptionDTO() {
    }

    public PrescriptionDTO(long id, String description, long idPatient, long idDoctor, LocalDate dateStr, int time, String priority) {
        this.description = description;
        this.id = id;
        this.idPatient = idPatient;
        this.idDoctor = idDoctor;
        this.dateStr = dateStr;
        this.time = time;
        this.priority = priority;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(long idPatient) {
        this.idPatient = idPatient;
    }

    public long getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(long idDoctor) {
        this.idDoctor = idDoctor;
    }

    public LocalDate getDate() {
        return dateStr;
    }

    public void setDate(LocalDate dateStr) {
        this.dateStr = dateStr;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
}
