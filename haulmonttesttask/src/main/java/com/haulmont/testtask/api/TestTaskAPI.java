package com.haulmont.testtask.api;

import com.haulmont.testtask.model.DAO;
import com.haulmont.testtask.model.impl.DAOimpl;
import com.haulmont.testtask.model.impl.DoctorDTO;
import com.haulmont.testtask.model.impl.PatientDTO;
import com.haulmont.testtask.model.impl.PrescriptionDTO;

import java.time.LocalDate;

public class TestTaskAPI {

    public static DoctorDTO[] doctors() {
        DAO dao = DAOimpl.getInstance();
        return dao.doctors();
    }

    public static void createDoctor(String firstName, String lastName, String fatherName, String specification) {
        DAO dao = DAOimpl.getInstance();
        dao.createDoctor(firstName, lastName, fatherName, specification);
    }

    public static void editDoctor(String id, String firstName, String lastName, String fatherName, String specification) {
        DAO dao = DAOimpl.getInstance();
        dao.editDoctor(Long.parseLong(id), firstName, lastName, fatherName, specification);
    }

    public static void deleteDoctor(String id) {
        DAO dao = DAOimpl.getInstance();
        dao.deleteDoctor(Long.parseLong(id));
    }

    public static PrescriptionDTO[] prescriptions() {
        DAO dao = DAOimpl.getInstance();
        return dao.prescriptions();
    }

    public static void createPrescription(String description, String idPatient, String idDoctor, LocalDate dateStr, String time, String priority) {
        DAO dao = DAOimpl.getInstance();
        dao.createPrescription(description, Long.parseLong(idPatient), Long.parseLong(idDoctor), dateStr, Integer.parseInt(time), priority);
    }

    public static void editPrescription(String id, String description, String idPatient, String idDoctor, LocalDate dateStr, String time, String priority) {
        DAO dao = DAOimpl.getInstance();
        dao.editPrescription(Long.parseLong(id), description, Long.parseLong(idPatient), Long.parseLong(idDoctor), dateStr, Integer.parseInt(time), priority);
    }

    public static void deletePrescription(String id) {
        DAO dao = DAOimpl.getInstance();
        dao.deletePrescription(Long.parseLong(id));
    }


    public static PatientDTO[] patients() {
        DAO dao = DAOimpl.getInstance();
        return dao.patients();
    }

    public static void createPatient(String firstName, String lastName, String fatherName, String phone) {
        DAO dao = DAOimpl.getInstance();
        dao.createPatient(firstName, lastName, fatherName, phone);
    }

    public static void editPatient(String id, String firstName, String lastName, String fatherName, String phone) {
        DAO dao = DAOimpl.getInstance();
        dao.editPatient(Long.parseLong(id), firstName, lastName, fatherName, phone);
    }

    public static void deletePatient(String id) {
        DAO dao = DAOimpl.getInstance();
        dao.deletePatient(Long.parseLong(id));
    }
}
